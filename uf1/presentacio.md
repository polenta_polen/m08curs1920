# Introducció a la meva App

 > **__Recorda que la presentació és molt important (un 25% de la nota de la UF1 i la prova de les presentacions és un altre 25%)__**
 
 <br>

## Calendari

**7/11** Presentacions individuals.

**11/11** Prova de totes les presentacions dels companys/companyes de classe.

<br><br>

## Informe de l'aplicació

#### Què necessito saber abans de fer la presentació

1. **PROPÒSIT** Per a què serveix l'aplicació.

3. **CARACTERÍSTIQUES** Descriu les característiques principals de la aplicació. Minim 8.

<br>

    Característica1: ...

    Descriptió1: ...

    Imatges1 (Com funciona): ...

<br>

    Característica2: ...

    Descriptió2: ...

    Imatges2 (Com funciona): ...
    
<br>

...

<br>

    Característica8: ...

    Descriptió8: ...

    Imatges8 (Com funciona): ...


4. **SITUACIONS** Pensa en 3 situacions diferents on pot ser útil l'aplicació

  - Situació1:  
  - Situació2:
  - Situació3:


5. **PRESENTACIÓ** fes una bona presentació per introduir la teva aplicació als companys (Slideshare, Presentacions o Prezi pot ser una bona opció)



  Coses a tindre en compte per la presentació:

    - Em presento a mi mateix i l'aplicació.

    - Què soluciona l'aplicació? Totes les apps estan fetes per solucionar una necessitat.

    - Contextualitzar l'aplicació: És Google, zoho, office...

    - Funcionalitat de l'aplicació.

    - Aplicacions similars al mercat, pros i contres de la nostra aplicació envers la competència.

    - Simulació d'ús de l'app.

    - Punts forts de l'aplicació.

    - Si fossis desenvolupador. Com miraries de millorar-la?


 
  La presentació ha de ser:

    - Utilitzar material gràfic i POC TEXT. MAI LLEGIR LA PRESENTACIÓ.
    
    - Interacciona amb els assistents.

    - Fes una simulació real de l'aplicació.

    - Pensa què puc explicar en 5 minuts que sigui entretingut i, al mateix temps, rigorós.
