# P3. Eines de Gestió de Grups 

### Gestió de grups

Durant 4 hores treballarem P3. Explorarem diferents eines per a la gestió de grups i l'organització de xarxes socials. 

Tractarem diferents aplicatius. Per cada un d'ells s'ha d'introduir en l'informe.

**És important:**
> + Explorar a Internet qualsevol punt que no sabem com funciona.<br>
> + Per a cada punt anar redactant l'informe que el professor avaluarà.<br>


Comparació entre  diferents organitzadors de grup/tasques

1. Fes un compte en **Asana, Trello i Redbooth** i realitza un testeig de TOTES les funcionalitats que hi vegis. Acte seguit realitza un petit informe amb avantatges i inconvenients que li trobes a cada aplicatiu web.

2. **Doodle.** Analitza aquest aplicatiu web i simula una quedada d'una reunió de la teva organització amb uns possibles proveïdors o clients. CAPTURES i explicació en l'informe.

3. **Gantt Project.** En entorns on s'ha de planificar un projecte és molt útil i molt comú utilitzar una eina que ajudi a veure en un diagrama com s'organitzen les tasques, qui és el responsable i en quin temps s'han de dur a terme. Realitza un diagrama de gantt amb aquesta eina que et serveixi per la teva organització. Pots realitzar un diagrama de Gantt del teu projecte de síntesi, per exemple (de setembre a juny) amb la previsió de les tasques, els tempos i els recursos que estimeu a dia d'avui destinar-hi. 
    
    Per instal·lar-ho descarregueu el fitxer (.deb) i executeu al terminal:
    
* [ ]     >  sudo dpkg -i paquete.deb
   
* [ ]      >  sudo apt-get -f install 
      
* [ ]   >  sudo dpkg -i paquete.deb



4. **MediaWiki.** En tota organització és necessari una wiki per a que tots els treballadors coneguin i modifiquin els punts comuns. Instal·la i configura MediaWiki i fes una pàgina amb les normes bàsiques de la teva organització. CAPTURES i explicació del que es va fent, guia't pels links d'ajuda que hi ha en el Moodle.

En qualsevol moment dins l'informe podeu incloure qualsevol contingut web significatiu com poden ser imatges o vídeos.

# Aquesta part la deixem PENDENT de moment.

### Eines xarxes socials

Explorarem ara diferents eines per a la gestió de les nostres **xarxes socials.**

Cal doncs, centrar-nos en els següents aplicatius analitzadors de xarxa social i descriure en l'informe com pot afectar en la nostra organització.

**És important:**
> + Explorar a Internet qualsevol punt que no sabem com funciona.
> + Per a cada punt anar redactant l'informe que el professor avaluarà.


Escull dos aplicatius i respon:
+ Analitza les funcionalitats que permet cada eina i anota-ho en l'informe. 
    
    Aplicatiu | Funcionalitats
    ---|---
    aplicatiu1 | funcionalitat 1 <br> funcionalitat 2 <br> funcionalitat3
    aplicatiu2 | funcionalitat 1 <br> funcionalitat 2

+ Quines xarxes socials pots controlar?
	
+ Què et permet fer per a cada xarxa social?

+ Per a què li pot interessar a la teva empresa/organització l'ús d'aquest aplicatiu?



Realitza **CAPTURES** del seu funcionament i un **LListat** de les seves funcionalitats principals.

Escull dos Aplicatius entre els següents o els links del moodle:

+ Tweetdeck
+ Hootsuite
+ HowSociable
+ Buffer.com

