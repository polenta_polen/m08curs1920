## Pràctica 3 (HTML)

[Enunciat](p3.md)

## Pràctica 4 (Mapa i Youtube) és a Moodle.

## Pràctica **AMAZON**

* 03-11-2019
    
    En aquesta sessió utilitzarem **CSS** per millorar l'aspecte visual de les nostres 3 pàgines de projecte, tot el codi CSS el possaràs a un fitxer apart a la ruta /css i es dirà amazonCOGNOM.css, per a carregar el CSS a totes les pàgines inclouràs la següent línia al ```<head>``` de les 3 webs:
    
    ```<link href="amazonCOGNOM.css" rel="stylesheet" type="text/css" />```

    1. Faràs servir com a base l'exemple de [W3Schools](https://www.w3schools.com/css/tryit.asp?filename=trycss_navbar_horizontal_black_active) per a millorar el menú de la pàgina web (Inici/Productes/Avís legal).
    2. Utilitzaràs un color de fons i una imatge agradables i que tinguin que veure amb els productes oferts.
    3. *(Not CSS but HTML)* Possaràs una icona a la teva web (una icona adient, es clar), afegint ```<link rel="icon" type="image/png" href="imagen.png">``` al ```<head>``` de les pàgines web.
        * Com a curiositat: Mira com es poden crear [imatges amb CSS](https://cssicon.space/#/icon/mustache)
    4. A la pàgina de productes crear una galeria de productes amb, com a mínim el següent: Nom de producte, imatge de producte, petita descripció del producte i preu del producte. Feu servir [aquest exemple](https://www.w3schools.com/css/tryit.asp?filename=trycss_image_gallery) per modificar-lo i adaptar-lo.
    
    

    
