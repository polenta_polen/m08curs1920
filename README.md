# M08Curs1920

## Curs 2019-2020 de M08-Aplicacions Web

* [UF1 - Ofimàtica i eines Web](/uf1/readme.md)
* [UF2 - Gestió d'arxius Web](/uf2/readme.md)
* [UF3 - Gestors de continguts](/uf3/readme.md)
* [UF5 - Fonaments HTML i CSS](/uf5/readme.md)
