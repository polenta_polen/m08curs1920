# CMS - Content Management System.

## Pràctica 1: 

Activitat d’anàlisi comparatiu de diferents CMS’s. (Feta a classe).

## Pràctica 2: 

Seleccioneu una empresa real que tingui similitud amb algun dels tres casos que heu treballat a la pràctica de cerca d'informació (Restaurant, Botiga online, Startup de projectes).

Quina empresa heu seleccionat?

* Jofre: SquareEnix (Squaresoft)

* Kilian: Steam

* Uiliam: Blizzard

* Santi: Cooler Master

* Dani: McDonalds

* Manu: VelscoTattoo

Un cop seleccioneu la marca/empresa podeu instal·lar Wordpress i començar a personalitzar la web amb les opcions que heu investigat (plugins).

**Obligatori:** Mostrar captura i/o explicació. 

* Canviar el Tema per defecte instal·lant-ne un.

* Instal·lar 5 plugins (justificant perquè els instal·leu).

* Crear un perfil de cada tipus que permeti Wordpress. Explicar què pot fer cada tipus de perfil.

* Canvia o afegeix algun canvi CSS a un Tema. Com ho has fet?

## Pràctica 3:

Feu la mateixa pàgina que teniu en Wordpress amb Joomla!

Tens un tutorial de com instal·lar Joomla [aqui](https://dungeonofbits.com/instalacion-de-joomla-en-linux.html).


## Pràctica 4: 

Creació d'un lloc Web amb Wordpress fet per una empresa real en grups de 2 persones.

1. Grup 1: Jofre i Santi - Web de l'associació d'estudiants.
2. Grup 2: Kilian i Manu - Web de projectes solidaris Ana.
3. Grup 3: Dani i Uiliam - Bar Coyote D7.

* Estudi inicial (a classe).
* Visita a l'empresa (horari extraescolar o escolar, segons projecte).
* Creació d'una pàgina web amb wordpress personalitzada que s'ajusti al projecte i els objectius perseguits pel client.

