## Práctica de git.

En esta práctica aprenderás a utilizar git con comandos de terminal.

## Recursos:

[Tutorial básico de git](https://dungeonofbits.com/utilizando-git-a-nivel-basico.html)

[Resumen de comandos git](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)


## Pasos a seguir.

Deberás mostrar una captura de pantalla con los comandos utilizados y el estado (status) del repositorio, para cada bloque de instrucciones.

1. Creación del repositorio:
    * Instala git en una MV o usa git en la màquina real.
    * Crea un directorio para en /home/USUARIO llamada nombre (dónde nombre es tu nombre).
    * Crea un repositorio local en el directorio anterior.
    * Muestra el estado.
    
2. Añadiendo ficheros a stage.
    * Crea un fichero en el directorio llamado "primero.md".
    * Añade el fichero al espacio intermedio (stage).
    * Muestra el estado.
    
3. Haciendo un commit:
    * Haz el primer commit con mensaje.
    * Muestra el estado.
    * Mira el historial de commits.

4. Segundo commit:
    * Modifica el fichero inicial.
    * Muestra el estado.
    * Añade el fichero cambiado a stage.
    * Muestra el estado.
    * Haz un segundo commit con mensaje.
    * Mira el historial de commits.

5. Añadiendo más ficheros y commit sin mensaje:
    * Crea dos ficheros nuevos en el repositorio llamados "segundo.md" y "tercero.md".
    * Haz un commit sin mensaje.
    * Escribe el mensaje (si estás en nano salva y sal).
    * Mira el historial de commits.

6. Eliminar ficheros de stage:
    * Crea un fichero llamado "cuarto.md".
    * Añádelo a stage.
    * Muestra el estado.
    * Quítalo de stage con(git reset).
    * Muestra el estado.

7. Mostrar diferencias entre versión en stage y versión en área de trabajo:
    * Modifica el fichero "segundo.md".
    * Añádelo a stage.
    * Vuelve a modificar el fichero.
    * Busca las diferencias con (git diff).

8. Clonar un repositorio de git:
    * Clona el repositorio https://gitlab.com/xsancho/m08curs1920 en el directorio /home/TuUsuario/M08UF2P3. (1)
    
```
git clone https://gitlab.com/xsancho/m08curs1920 M08UF2P3
```
    * Modifica el fichero p3.md que es ESTA PRÁCTICA y pon tus respuestas a la práctica.
    * Sube la práctica a tu gitlab con git push (necesitas tu nombre de usuario y password de gitlab) (2)
```
git push -u  https://gitlab.com/TUUSUARIO/PROYECTO master
```
    * Muestra el resultado en gitlab.
