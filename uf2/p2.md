# Instalación de Owncloud en Linux:

En esta práctica instalarás el servicio Owncloud en una máquina virtual con OS Linux Ubuntu 18.04 LTS.

Para instalar el servicio deberás seguir este tutorial:

[Instalación Owncloud](https://dungeonofbits.com/instalacion-de-owncloud-en-linux.html)

Para que los demás miembros de la clase puedan acceder a tu servicio deberás tener la MV en modo **Adaptador puente**.

Para cada uno de los siguientes pasos se pide una **captura de pantalla**:

1. Demuestra que has instalado owncloud en tu MV, para ello muestra captura de pantalla de tu IP y del navegador con owncloud. **Importante** Cambia el prompt de tu MV para que aparezca tu nombre en él siguiendo estas [indicaciones](https://www.linuxtotal.com.mx/index.php?cont=info__tips_017)
2. Crea un grupo llamado **aula84**.
3. Crea un usuario para cada miembro de la clase perteneciente al grupo **aula84**, donde el nombre de usuario y la contraseña sea su apellido.
4. Crea un directorio llamado como tu nombre y dale acceso a los miembros del grupo **aula84**.
5. Accede desde tu PC al owncloud de cada compañero/a y añade un fichero txt donde le felicitas la Navidad en el directorio raiz de Ownclou, el nombre del fichero será **nombre.txt**, donde nombre será tu propio nombre.
6. Accede desde tu móvil a la página de owncloud de cada compañero/a y en la carpeta compartida (que se llama como el nombre de el/la compañero/a) subirás una foto hecha con tu propio móvil (por favor nada raro). El SSID de la WiFi és **SMX2TLoPeta** y el password és **AaBbCcDdEe**. 
7. *opcional* Investiga sobre el almacenamiento externo de owncloud e intenta añadir alguno a tu servicio.

Si no se indica lo contrario cada parte de la práctica tiene el mismo valor. La parte opcional añade un punto extra a la práctica.

